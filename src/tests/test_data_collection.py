import os
import pathlib
import pandas as pd
import numpy as np
from numpy.testing import assert_allclose

import src.tools.data_collection_tools as data_collection_tools


def test_load_misr_phproj_df():
    csv = data_collection_tools.load_misr_phproj_df()
    target_len = 43009
    assert csv.shape[0] == target_len


def test_generate_misr_url():
    df = data_collection_tools.load_misr_phproj_df()
    generated_url = data_collection_tools.generate_misr_url(df.iloc[0, :])
    tgt_url = "https://asdc.larc.nasa.gov/documents/misr/plume/Global-2018_Aug/099476/Plumes_O099476-B094-SPWB01.txt"
    assert tgt_url == generated_url


def test_retreive_misr_plume_polygon():
    url = "https://asdc.larc.nasa.gov/documents/misr/plume/Global-2018_Aug/099476/Plumes_O099476-B094-SPWB01.txt"
    dest = pathlib.Path(__file__).parent / 'files'
    data_collection_tools.retreive_misr_plume_polygon(url, dest)

    target_lons = np.array([31.84, 31.809, 31.79, 31.764, 31.761, 31.769, 31.793, 31.818, 31.833, 31.838])
    target_lats = np.array([-4.146, -4.128, -4.119, -4.125, -4.142, -4.16, -4.17, -4.174, -4.173, -4.163])

    split_url = url.split('/')
    outpath = os.path.join(dest, split_url[-3], split_url[-1].replace('.txt', '_polygon.csv'))
    df = pd.read_csv(outpath)
    os.remove(outpath)

    assert_allclose(df.lats.values, target_lats)
    assert_allclose(df.lons.values, target_lons)


def test_generate_modis_l1_fname_prefix():
    df = data_collection_tools.load_misr_phproj_df()
    generated_prefix = data_collection_tools.generate_modis_l1_fname_prefix(df.iloc[0, :])
    target_prefix = 'MOD02HKM.A2018243.0815'
    assert generated_prefix == target_prefix
    target_prefix = 'MOD03.A2018243.0815'
    generated_prefix = data_collection_tools.generate_modis_l1_fname_prefix(df.iloc[0, :], tag="MOD03.A")
    assert generated_prefix == target_prefix


def test_generate_modis_hv_fname_prefix():
    df = data_collection_tools.load_misr_phproj_df()
    target_prefix = "MCD19A2.A2018243.h21v09"
    row = df.iloc[0, :]
    h = 21
    v = 9
    generated_prefix = data_collection_tools.generate_modis_hv_fname_prefix(row, h, v)
    assert target_prefix == generated_prefix
