import numpy as np
from numpy.testing import assert_equal

import src.tools.data_preparation_tools as data_preparation_tools


def test_lat_lon_to_x_y():
    lats = np.arange(0, 10, 0.5)
    lons = np.arange(0, 10, 0.5)
    lats, lons = np.meshgrid(lats, lons)
    lat_lon_grid = np.dstack([lats, lons])
    target_lats = np.array([0, 1.5, 4.5, 7.5, 9])
    target_lons = np.array([0, 1.5, 4.5, 7.5, 9])
    target_x = [0, 3, 9, 15, 18, 0]
    target_y = [0, 3, 9, 15, 18, 0]

    x, y = data_preparation_tools.lat_lon_to_x_y(lat_lon_grid, target_lats, target_lons)
    assert_equal(x, target_x)
    assert_equal(y, target_y)
