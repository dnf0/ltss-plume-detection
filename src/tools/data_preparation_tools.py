import os
import re
from dotenv import load_dotenv, find_dotenv
import calendar
import numpy as np
import pandas as pd
from pyhdf.SD import SD, SDC
import scipy.ndimage as ndimage
from PIL import Image
import sklearn.decomposition as decomposition
from skimage.measure import label, regionprops
import pyresample as pr
import pyproj
import matplotlib.pyplot as plt
from matplotlib.path import Path

from src.tools.data_collection_tools import load_misr_phproj_df

load_dotenv(find_dotenv())


def lat_lon_to_x_y(lat_lon_grid, target_lons, target_lats):
    """

    Args:
        lat_lon_grid (ndarray): Matrix of stacked lat lon coordinates
        target_lats (ndarray): Vector of lats to be matched
        target_lons (ndarray): Vector of lons to be matched

    Returns:
        x_coords: Vector of x coordinates for input lat/lon grid
        y_coords: Vector of y coordinates for input lat/lon grid

    """
    x_coords = []
    y_coords = []

    for t_lat, t_lon in zip(target_lats, target_lons):
        abs_diff = np.abs(lat_lon_grid - (t_lat, t_lon))
        total_diff = np.sum(abs_diff, axis=2)
        min_loc = np.argmin(total_diff)

        x_coords.append(min_loc % lat_lon_grid.shape[1])
        y_coords.append(min_loc // lat_lon_grid.shape[1])

    # close polygon
    x_coords.append(x_coords[0])
    y_coords.append(y_coords[0])

    # convert to numpy arrays
    y_coords = np.array(y_coords)
    x_coords = np.array(x_coords)

    return x_coords, y_coords


def match_plumes_to_timestamp(target_datetime):
    # find matches from plume height summary data
    misr_ph_df = load_misr_phproj_df()
    misr_ph_df['dt'] = pd.to_datetime(misr_ph_df['DateAcquired'] + " " + misr_ph_df['TimeAcquired'])
    keepers = ((np.abs(misr_ph_df['dt'] - target_datetime) <= pd.Timedelta(minutes=5)) &
               (misr_ph_df['dt'] >= target_datetime))
    return misr_ph_df[keepers]


def locate_plumes_in_image(target_datetime, lat_lon):
    """
    For a given modis image find a plumes associated with the
    image and return thier location in image coordintes using
    geo coordinates.

    Args:
        image_stub (str):

    Returns:

    """
    matched_plumes = match_plumes_to_timestamp(target_datetime)

    # generate path to plume data
    month_tag = f"Global-{target_datetime.year}_{calendar.month_abbr[target_datetime.month]}"
    target_dir = os.path.join(os.environ.get("MISR_PLUME_POLY_DEST"), month_tag)

    # iterate over each plume and locate polygon position in image
    plume_polygons_dict = {}
    for i, plume_summary in matched_plumes.iterrows():
        misr_poly_fname = f"Plumes_{plume_summary['AerosolRgnName']}_polygon.csv"
        plume_csv_filepath = os.path.join(target_dir, misr_poly_fname)
        if os.path.exists(plume_csv_filepath):
            plume_df = pd.read_csv(plume_csv_filepath)
        else:
            continue
        x_coords, y_coords = lat_lon_to_x_y(lat_lon, plume_df.lons, plume_df.lats)

        plume_polygons_dict[plume_summary['AerosolRgnName']] = {
            'plume_lats': plume_df.lats,
            'plume_lons': plume_df.lons,
            'y_coords': y_coords,
            'x_coords': x_coords
        }

    return plume_polygons_dict


def group_plumes(plume_polygons):
    """
    Identifies plumes that are clustered but taking mean location
    and finding all plumes that are withing MAX_DIST of each other.

    Each plume can only appear in a single group

    Args:
        plume_polygons (dict): plume coordinates

    Returns:
        list: plume groups

    """
    centroids = []
    plume_id = []
    for k in plume_polygons:
        centroids.append((plume_polygons[k]['x_coords'].mean(),
                          plume_polygons[k]['y_coords'].mean()))
        plume_id.append(k)

    centroids = np.array(centroids)
    plume_id = np.array(plume_id)

    MAX_DIST = 200  # assuming 500m pixels, then ~ 20km  # TODO move to constants
    assigned = []  # check which plumes have been checked for proximity
    grouped = []  # keeps track os which plumes are clustered together
    for i, pc in enumerate(centroids):

        if i in assigned:
            continue

        # get distances
        distances = np.sqrt(np.sum((centroids - pc) ** 2, axis=1))

        # in range
        in_range = np.where(distances <= MAX_DIST)[0]

        group = []
        for ir in in_range:
            assigned.append(ir)
            group.append(plume_id[ir])
        grouped.append(group)

    return grouped


def scale_to_8bit(im):
    im -= im.min()
    return np.floor_divide(im, (im.max() - im.min()) / 255, casting='unsafe').astype('uint8')


def save_image(prefix, extension, i, image, im_coords, buffer_coords):
    min_y = im_coords['min_y'] - buffer_coords['min_y']
    max_y = im_coords['max_y'] + buffer_coords['max_y']
    min_x = im_coords['min_x'] - buffer_coords['min_x']
    max_x = im_coords['max_x'] + buffer_coords['max_x']

    # subset data
    image = np.dstack([scale_to_8bit(im[min_y:max_y, min_x:max_x]) for im in image])
    im = Image.fromarray(image)

    outpath = os.path.join(os.environ.get("ANNOTATION_IMAGE_DEST"), extension)
    outfile = prefix + "_" + extension + "_" + str(i) + ".png"

    if not os.path.exists(outpath):
        os.makedirs(outpath)

    im.save(os.path.join(outpath, outfile), compress_level=0)


def save_annotation(prefix, style, extension, i, annotation):
    outpath = os.path.join(os.environ.get("ANNOTATION_DEST"), style, extension)
    outfile = prefix + "_" + extension + "_" + str(i) + ".txt"

    if not os.path.exists(outpath):
        os.makedirs(outpath)

    with open(os.path.join(outpath, outfile), 'w+') as f:
        f.write(" ".join(annotation))


def decompose_pca(b1, b2):
    # normalise
    mean_b1, std_b1 = np.mean(b1), np.std(b1)
    b1 = (b1 - mean_b1) / std_b1
    mean_b2, std_b2 = np.mean(b2), np.std(b2)
    b2 = (b2 - mean_b2) / std_b2

    for_pca = np.stack([b1, b2])
    ny, nx = b1.shape
    pca = decomposition.PCA(n_components=1)
    for_pca = np.reshape(for_pca, (2, nx * ny))
    pca_decomp = pca.fit(for_pca)
    return np.reshape(pca_decomp.components_, (ny, nx))


def construct_mask(pp, bounds):
    extent = [[x - bounds['min_x'], y - bounds['min_y']] for x, y in zip(pp['x_coords'], pp['y_coords'])]

    size_x = bounds['max_x'] - bounds['min_x']
    size_y = bounds['max_y'] - bounds['min_y']
    x, y = np.meshgrid(np.arange(size_x), np.arange(size_y))
    x, y = x.flatten(), y.flatten()
    points = np.vstack((x, y)).T

    # create mask
    path = Path(extent)
    mask = path.contains_points(points)
    mask = mask.reshape((size_y, size_x))
    return mask


def plume_class(pp, im_coords, products, k):
    # create mask for plume
    plume_mask = construct_mask(pp, im_coords)

    # generate labels
    plume_labels = label(plume_mask)
    maiac_subset = products['maiac'][im_coords['min_y']:im_coords['max_y'], im_coords['min_x']:im_coords['max_x']]
    fire_subset = products['fires'][im_coords['min_y']:im_coords['max_y'], im_coords['min_x']:im_coords['max_x']]
    assert plume_labels.shape == maiac_subset.shape == fire_subset.shape
    regions = regionprops(plume_labels, intensity_image=maiac_subset)
    assert len(regions) == 1
    region = regions[0]

    # calculate the statistics
    axis_ratio = region.major_axis_length / region.minor_axis_length
    valid_maiac_proportion = np.sum(maiac_subset[plume_mask] > 0) / np.sum(plume_mask)
    fires_present = fire_subset.astype(bool).any()
    background_aod = np.mean(maiac_subset[~plume_mask & (maiac_subset > 0)])
    plume_aod = np.mean(maiac_subset[plume_mask & (maiac_subset > 0)])

    # save the stats to file
    outpath = os.path.join(os.environ.get("PLUME_STATS_DEST"))
    outfile = "plume_stats.txt"

    if not os.path.exists(outpath):
        os.makedirs(outpath)
    stats = [str(s) for s in
             [k, axis_ratio, valid_maiac_proportion, fires_present, background_aod, plume_aod, plume_mask.sum(), "\n"]]
    with open(os.path.join(outpath, outfile), 'a+') as f:
        f.write(" ".join(stats))

    # convert to flags
    axis_ratio = axis_ratio > 2.5
    valid_maiac_proportion = valid_maiac_proportion > 0.9
    maiac_above_background = plume_aod > background_aod

    # BACKGROUND CLASS
    if not maiac_above_background:
        return '6'

    # VALID AXIS RATIO CLASSES
    # valid axis ratio, maiac coverage and fire present
    elif axis_ratio & valid_maiac_proportion & fires_present:
        return '0'
    # valid axis ratio and maiac coverage, no fire
    elif axis_ratio & valid_maiac_proportion & ~fires_present:
        return '1'
    # valid axis ratio, invalid maiac coverage, fire or no fire.
    elif axis_ratio & ~valid_maiac_proportion:
        return '2'

    # INVALID LENGTH RATIO CLASSES
    # valid maiac coverage and fire
    elif valid_maiac_proportion & fires_present:
        return '3'
    # valid maiac coverage only
    elif valid_maiac_proportion:
        return '4'
    # fire only
    else:
        return '5'


def generate_images_annotations(prefix, products, plume_polygons, groups, quicklook=False):
    IM_BUF = 20
    h, w = products['blue'].shape

    # setup images, scaling to 8 bit
    # BLUE, SWIR, MWIR
    image_a = [products['blue'], products['nir'], products['mwir']]
    extension_a = "bl_sw_mw"

    # BLUE, MAIAC, SWIR/MWIR PCA
    pca = decompose_pca(products['nir'], products['mwir'])
    image_b = [products['blue'], products['maiac'], pca]
    extension_b = "bl_mc_pc"

    # BLUE, MAIAC FIRES
    image_c = [products['blue'], products['maiac'], products['fires']]
    extension_c = "bl_mc_fm"

    # MAIAC ONLY
    maiac_fires = products['maiac'].copy()
    fire_mask = products['fires'].astype(bool)
    maiac_fires[fire_mask] = products['maiac'].max()  # to ensure fires get set to 255
    image_d = [maiac_fires, products['maiac'], products['maiac']]
    extension_d = "mc_mc_mc"

    images = [image_a, image_b, image_c, image_d]
    extensions = [extension_a, extension_b, extension_c, extension_d]

    # iterate over each set of plume polygons
    for i, group in enumerate(groups):

        im_coords = {}
        im_coords['min_y'] = 9999
        im_coords['min_x'] = 9999
        im_coords['max_y'] = 0
        im_coords['max_x'] = 0

        # find image subset
        for k in group:
            pp = plume_polygons[k]
            if pp['y_coords'].min() < im_coords['min_y']:
                im_coords['min_y'] = pp['y_coords'].min()
            if pp['y_coords'].max() > im_coords['max_y']:
                im_coords['max_y'] = pp['y_coords'].max()
            if pp['x_coords'].min() < im_coords['min_x']:
                im_coords['min_x'] = pp['x_coords'].min()
            if pp['x_coords'].max() > im_coords['max_x']:
                im_coords['max_x'] = pp['x_coords'].max()

        # get axis buffers
        buf_size = {}
        buf_size['min_y'] = IM_BUF if im_coords['min_y'] - IM_BUF > 0 else im_coords['min_y']
        buf_size['max_y'] = IM_BUF if im_coords['max_y'] + IM_BUF < h else h - im_coords['max_y'] - 1
        buf_size['min_x'] = IM_BUF if im_coords['min_x'] - IM_BUF > 0 else im_coords['min_x']
        buf_size['max_x'] = IM_BUF if im_coords['max_x'] + IM_BUF < w else w - im_coords['max_x'] - 1

        # save image outputs
        for ext, im in zip(extensions, images):
            save_image(prefix, ext, i, im, im_coords, buf_size)

        # get plume group classes
        classes = []
        for k in group:
            pp = plume_polygons[k]
            classes.append(plume_class(pp, im_coords, products, k))

        for ext in extensions:

            # generate annotation outputs
            coco_annotation = [prefix + "_" + str(i) + ".png"]
            subset_height = (im_coords['max_y'] + buf_size['max_y']) - (im_coords['min_y'] - buf_size['min_y'])
            subset_width = (im_coords['max_x'] + buf_size['max_x']) - (im_coords['min_x'] - buf_size['min_x'])

            for k, c in zip(group, classes):

                pp = plume_polygons[k]
                py = pp['y_coords'] - im_coords['min_y'] + buf_size['min_y']
                px = pp['x_coords'] - im_coords['min_x'] + buf_size['min_x']

                # append to coco annotation for group
                coco_annotation.append(str(px.min()))  # x min
                coco_annotation.append(str(py.min()))  # y min
                coco_annotation.append(str(px.max()))  # x max
                coco_annotation.append(str(py.max()))  # y max
                coco_annotation.append(c)

                # write out yolo
                pw = ((px.max() - px.min()) / 2) / subset_width
                ph = ((py.max() - py.min()) / 2) / subset_height
                x_centre = (px.min() + pw) / subset_width
                y_centre = (py.min() + ph) / subset_height
                if pw == 0 or ph == 0:
                    continue
                # 0:8 index to limit number of decimal places
                yolo_annotation = [str(j)[0:8] for j in [c, str(x_centre), y_centre, pw, ph]] + ['\n']
                save_annotation(prefix, 'yolo', ext, i, yolo_annotation)

            # save coco annotation
            save_annotation(prefix, 'coco', ext, i, coco_annotation)

        if quicklook:
            for ext, im, im_index in zip(extensions, images, [0, 1, 1, 0]):
                save_quicklook(im[im_index], group, classes, im_coords, buf_size, plume_polygons, ext, prefix, i)


def save_quicklook(image, group, classes, im_coords, buffer_coords, plume_polygons, extension, prefix, i):
    class_colors = {
        "0": 'aqua',  # passes all metrics (fire, axis ratio, maiac coverage, maiac >bg)
        "1": 'navy',  # no fire, valid axis ratio and maiac coverage, maiac >bg
        "2": 'purple',  # valid axis ratio, fire/no fire and invalid maiac coverage, maiac >bg
        "3": 'orange',  # invalid axis ratio, fire and valid maiac coverage, maiac >bg
        "4": 'yellow',  # invalid axis ratio, not fires, valid maiac coverage, maiac >bg
        "5": 'green',  # invalid axis ratio, invalid coverage, fire only, maiac >bg
        "6": "red",  # maiac <=bg
    }

    min_y = im_coords['min_y'] - buffer_coords['min_y']
    max_y = im_coords['max_y'] + buffer_coords['max_y']
    min_x = im_coords['min_x'] - buffer_coords['min_x']
    max_x = im_coords['max_x'] + buffer_coords['max_x']

    # subset data
    # image = np.dstack([scale_to_8bit(im[min_y:max_y, min_x:max_x]) for im in image])
    image = image[min_y:max_y, min_x:max_x]
    plt.imshow(image, interpolation='none', vmin=0)

    for k, c in zip(group, classes):
        pp = plume_polygons[k]
        py = pp['y_coords'] - im_coords['min_y'] + buffer_coords['min_y']
        px = pp['x_coords'] - im_coords['min_x'] + buffer_coords['min_x']
        plt.plot(px, py, class_colors[c])

    outpath = os.path.join(os.environ.get("QUICKLOOK_DEST"), extension)
    outfile = prefix + "_" + extension + "_" + str(i) + ".png"

    if not os.path.exists(outpath):
        os.makedirs(outpath)
    plt.savefig(os.path.join(outpath, outfile), bbox_inches='tight')
    plt.close()


def is_processed(modis_prefix):
    processed_images = os.listdir(os.environ.get("ANNOTATION_IMAGE_DEST"))
    return any([modis_prefix in f for f in processed_images])


def load_product(target_datetime, tag):
    # load in MODIS grid data use REGEX to match the stub
    mod_dir = os.path.join(os.environ.get("MOD_DEST"),
                           str(target_datetime.year),
                           str(target_datetime.day_of_year).zfill(3))
    mod_files = os.listdir(mod_dir)
    files = [f for f in mod_files if f"{tag}{target_datetime.strftime('%Y%j.%H%M')}" in f]
    print(mod_dir, files)
    assert len(files) == 1
    f_image = SD(os.path.join(mod_dir, files[0]), SDC.READ)
    return f_image


def zoom_to_hkm(arr, order=1):
    return ndimage.zoom(arr, 2, order=order)


def load_maiac(target_datetime, lat_lon):
    # generate target ky associated with MOD product, e.g. "20182430815T"
    target_key = str(target_datetime.year) + \
                 str(target_datetime.day_of_year).zfill(3) + \
                 str(target_datetime.hour).zfill(2) + \
                 str(target_datetime.minute).zfill(2) + "T"

    # find all maiac files in target dir
    mod_dir = os.path.join(os.environ.get("MOD_DEST"),
                           str(target_datetime.year),
                           str(target_datetime.day_of_year).zfill(3))
    mod_files = os.listdir(mod_dir)
    files = [f for f in mod_files if "MCD19A2" in f]

    # set up resampling grid and try to resample each file.  If not overlap,
    # catch error and continue
    modhkm_swath_def = pr.geometry.SwathDefinition(lons=lat_lon[..., 1], lats=lat_lon[..., 0])
    resampled = []
    for f in files:
        try:
            hdf_file = SD(os.path.join(mod_dir, f), SDC.READ)
            aod_dict, lat, lon = read_maiac_aod(hdf_file)

            aod = aod_dict[target_key]
            aod = np.ma.masked_array(aod, aod < 0)
            maiac_swath_def = pr.geometry.SwathDefinition(lons=lon, lats=lat)
            result = pr.kd_tree.resample_nearest(maiac_swath_def,
                                                 aod,
                                                 modhkm_swath_def, radius_of_influence=1000, epsilon=0.5)
            # replace -999 null values to avoid scaling issues
            result[result < 0] = -1
            resampled.append(result)

        except pr.geometry.IncompatibleAreas:
            continue
        except KeyError:
            continue

    # merge
    return np.sum(resampled, axis=0)


def read_maiac_aod(hdf_file):
    """

    :param hdf_file: an open MAIAC hdf file
    :return: a dictionary of all the aod images contained in the file and the lat and lon grids
    """

    # set up data dict
    dd = {}

    # Read global attribute.
    fattrs = hdf_file.attributes(full=1)
    # need to select the most appropriate layer in the product
    timestamps = fattrs['Orbit_time_stamp'][0].split(' ')
    timestamps = [t for t in timestamps if t != '']  # valid timestamps

    # if len(timestamps) > 4:
    #    timestamps = [t for t in timestamps if 'A' in t]
    #    timestamps = [timestamps[0]]

    for i, timestamp in enumerate(timestamps):
        # extract time
        t = re.search("[0-9]{11}[A-Z]{1}", timestamp).group()

        # Read dataset.
        aod = hdf_file.select('Optical_Depth_055')[i, :, :] * 0.001  # aod scaling factor
        aod[aod < 0] = -999  # just get rid of the filled values for now

        dd[t] = aod

    ga = fattrs["StructMetadata.0"]
    gridmeta = ga[0]
    # Construct the grid.  The needed information is in a global attribute
    # called 'StructMetadata.0'.  Use regular expressions to tease out the
    # extents of the grid.
    ul_regex = re.compile(r'''UpperLeftPointMtrs=\(
                                      (?P<upper_left_x>[+-]?\d+\.\d+)
                                      ,
                                      (?P<upper_left_y>[+-]?\d+\.\d+)
                                      \)''', re.VERBOSE)
    match = ul_regex.search(gridmeta)
    x0 = np.float(match.group('upper_left_x'))
    y0 = np.float(match.group('upper_left_y'))

    lr_regex = re.compile(r'''LowerRightMtrs=\(
                                      (?P<lower_right_x>[+-]?\d+\.\d+)
                                      ,
                                      (?P<lower_right_y>[+-]?\d+\.\d+)
                                      \)''', re.VERBOSE)
    match = lr_regex.search(gridmeta)
    x1 = np.float(match.group('lower_right_x'))
    y1 = np.float(match.group('lower_right_y'))
    ny, nx = aod.shape
    xinc = (x1 - x0) / nx
    yinc = (y1 - y0) / ny

    x = np.linspace(x0, x0 + xinc * nx, nx)
    y = np.linspace(y0, y0 + yinc * ny, ny)
    xv, yv = np.meshgrid(x, y)

    # In basemap, the sinusoidal projection is global, so we won't use it.
    # Instead we'll convert the grid back to lat/lons.
    sinu = pyproj.Proj("+proj=sinu +R=6371007.181 +nadgrids=@null +wktext")
    wgs84 = pyproj.Proj("+init=EPSG:4326")
    lon, lat = pyproj.transform(sinu, wgs84, xv, yv)

    return dd, lat, lon


def data_setup(target_datetime):
    products = {}

    # load half km
    hkm_product = load_product(target_datetime, "MOD02HKM.A")
    hkm_datasets = hkm_product.select('EV_500_RefSB')
    hkm_factors = hkm_datasets.attributes()['reflectance_scales']
    products['blue'] = hkm_datasets.get()[0, :, :] * hkm_factors[0]
    # products['swir'] = hkm_datasets.get()[-1, :, :] * hkm_factors[-1]

    # USING NIR instead of SWIR
    hkm_datasets = hkm_product.select('EV_250_Aggr500_RefSB')
    hkm_factors = hkm_datasets.attributes()['reflectance_scales']
    products['nir'] = hkm_datasets.get()[-1, :, :] * hkm_factors[-1]  # <- this is NIR

    # load geo
    geo_product = load_product(target_datetime, "MOD03.A")
    image_lats = zoom_to_hkm(geo_product.select('Latitude').get()[:])
    image_lons = zoom_to_hkm(geo_product.select('Longitude').get()[:])
    products['lat_lon'] = np.dstack([image_lats, image_lons])

    # load 1 km
    km_product = load_product(target_datetime, "MOD021KM.A")
    mwir = km_product.select('EV_1KM_Emissive').get()[1, :, :]
    products['mwir'] = zoom_to_hkm(mwir, order=0)

    # load MAIAC
    products['maiac'] = load_maiac(target_datetime, products['lat_lon'])

    # load fires
    fire_product = load_product(target_datetime, "MOD14.A")
    # https://modis-fire.umd.edu/files/MODIS_C6_Fire_User_Guide_A.pdf section 5.1.1
    fire_mask = (fire_product.select("fire mask").get()[:] >= 7).astype(int)
    products['fires'] = zoom_to_hkm(fire_mask, order=0)

    return products


def prepare_annotations(modis_prefix):
    # check if annotation_image already exists for stub
    if is_processed(modis_prefix):
        return

    split_stub = modis_prefix.split(".")
    target_datetime = pd.to_datetime(split_stub[1].strip('A') + split_stub[2], format="%Y%j%H%M")

    products = data_setup(target_datetime)
    plume_polygons = locate_plumes_in_image(target_datetime, products['lat_lon'])
    grouped_plumes = group_plumes(plume_polygons)
    generate_images_annotations(modis_prefix, products, plume_polygons, grouped_plumes, quicklook=True)
