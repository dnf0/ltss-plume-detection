#!/home/users/dnfisher/.conda/envs/ltss/bin/python
import pandas as pd
import os
import pathlib
import calendar
import io
from requests import get
import shutil
import sys
import ssl
import subprocess
from urllib.request import urlopen, Request, URLError, HTTPError
import urllib3
import csv
import math
import numpy as np

import src.resources.misr_phproj_vars as misr_vars


def myfloor(x, base=5):
    return int(base * math.floor(float(x) / base))


def load_misr_phproj_df():
    """
    Loads MISR plume height project CSV which forms the
    basis of the data used in the project (.e.g for extracting
    other resources such as MODIS L1B data)

    :return: pd.DataFrame
    """
    fp = pathlib.Path(__file__).parent.parent / 'resources' / 'misr_phproj_summary.txt'
    df = pd.read_csv(fp, sep="\\s+", error_bad_lines=False)
    df.columns = misr_vars.COL_NAMES

    return df


def load_hv():
    # add sin grid HV columns
    fp = pathlib.Path(__file__).parent.parent / 'resources' / 'sn_bound_10deg.txt'
    hv_data = np.genfromtxt(fp,
                            skip_header=7,
                            skip_footer=3)
    return hv_data


def find_hv(hv_data, lat, lon):
    """
    For a given lat lon arrays finds associated SIN grid cell.
    Note: There can be multiple grid cells associated with a lat
    lon location, but here the first is taken.
    Args:
        hv_data: hv sing grid datat
        lats: target lats (plume source latitude locations)
        lons: target lons (plume source longitude locations)

    Returns:
        hs: an array of hortizontal grid cells
        vs: an array of vertical grid cells

    """

    indexes = np.where((lat <= hv_data[:, 5]) &
                       (lat >= hv_data[:, 4]) &
                       (lon <= hv_data[:, 3]) &
                       (lon >= hv_data[:, 2]))[0]
    if indexes.size == 1:
        vs = hv_data[indexes, 0].astype(int)
        hs = hv_data[indexes, 1].astype(int)
    else:
        vs = [hv_data[index, 0].astype(int) for index in indexes]
        hs = [hv_data[index, 1].astype(int) for index in indexes]
    return hs, vs


def generate_misr_url(row):
    """ For a given MISR plume height summary dataset
    generate the URL that contained the associated data

    Args:
        row (pd.series): MISR plume height digitisation summary

    Returns:
        str: a url pointing to the MISR plume height digitisation inforamtion
    """
    year, month, day = row["DateAcquired"].split("-")
    month_abreviation = calendar.month_abbr[int(month)]
    region = row['AerosolRgnName']
    orbit = str(region.split("-")[0])
    url_suffix = os.path.join(str(year) + "_" + month_abreviation,
                              orbit.strip("O"),
                              "Plumes_" + region + ".txt")
    url_root = 'https://asdc.larc.nasa.gov/documents/misr/plume/Global-'
    return url_root + url_suffix


def retreive_misr_plume_polygon(url, destination):
    """ Loads the MISR plume height project URL and
    extracts the polygon from the data text.

    Args:
        url (str): URL pointing at the plume height project file

    Returns:
        pd.DataFrame: The lat lon coordinates that define the plume polygon
    """
    split_url = url.split('/')
    outpath = os.path.join(destination, split_url[-3], split_url[-1].replace('.txt', '_polygon.csv'))
    if os.path.exists(outpath):
        return None

    response = get(url)
    text = response.text.split('\n')
    capture = False
    captured = []

    # the text files have generic structure and
    # we can capture what we need between the POLYGON
    # and DIRECTION sections
    for line in text:
        if "POLYGON" in line:
            capture = True
        if "DIRECTION" in line:
            capture = False

        if capture:
            if line:
                captured.append(line)
    if captured:
        captured = captured[4:]
        df = pd.read_csv(io.StringIO('\n'.join(captured)), delim_whitespace=True)
        df.columns = ['ind', 'lons', 'lats', 'Block', 'Sample', 'Line']

        # make destination directory if it doesn't exist
        if not os.path.exists(os.path.join(destination, split_url[-3])):
            os.makedirs(os.path.join(destination, split_url[-3]))

        df.to_csv(outpath)
    return None


def generate_modis_l1_fname_prefix(row, tag="MOD02HKM.A"):
    """
    Generate MODIS L1 filename prefix

    Args:
        row (pd.DataFrame): containing various information from MISR product

    Returns:
        str: Formatted MODIS Terra Filename

    """
    dt = pd.to_datetime(row["DateAcquired"] + " " + row['TimeAcquired'])
    rounded_minute = myfloor(dt.minute)  # round MISR to nearest MODIS granule
    mod_f = tag + str(dt.year) + str(dt.dayofyear).zfill(3) + "." + str(dt.hour).zfill(2) + str(rounded_minute).zfill(2)
    return mod_f


def generate_modis_hv_fname_prefix(row, h, v, tag="MCD19A2.A"):
    """
    Generate MODIS Sin Grid filename prefix

    Args:
        row (pd.Series): plume information
        h (int): h coordinate
        v (int): v coordinate
        tag (str): MODIS target product

    Returns:
        str: Formatted MODIS Sin Grid Filename

    """
    dt = pd.to_datetime(row["DateAcquired"])
    y = dt.year
    doy = str(dt.dayofyear).zfill(3)
    h = str(h).zfill(2)
    v = str(v).zfill(2)
    return f"{tag}{y}{doy}.h{h}v{v}"


def geturl(url, out=None):
    """
    Retrieves a target URL

    Args:
        url (str): The target URL to retrieve
        out (str): The directory to sync

    Returns:
        None

    """
    headers = {'user-agent': os.environ.get("LAADS_USERAGENT") + sys.version.replace('\n', '').replace('\r', '')}
    headers['Authorization'] = 'Bearer ' + os.environ.get("LAADS_TOKEN")

    try:
        CTX = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
        if sys.version_info.major == 2:
            try:
                fh = urllib3.urlopen(urllib3.Request(url, headers=headers), context=CTX)
                if out is None:
                    return fh.read()
                else:
                    shutil.copyfileobj(fh, out)
            except urllib3.HTTPError as e:
                print('HTTP GET error code: %d' % e.code(), file=sys.stderr)
                print('HTTP GET error message: %s' % e.message, file=sys.stderr)
            except urllib3.URLError as e:
                print('Failed to make request: %s' % e.reason, file=sys.stderr)
            return None

        else:
            try:
                fh = urlopen(Request(url, headers=headers), context=CTX)
                if out is None:
                    return fh.read().decode('utf-8')
                else:
                    shutil.copyfileobj(fh, out)
            except HTTPError as e:
                print(e)
            except URLError as e:
                print('Failed to make request: %s' % e.reason, file=sys.stderr)
            return None

    except AttributeError:
        # OS X Python 2 and 3 don't support tlsv1.1+ therefore... curl
        try:
            args = ['curl', '--fail', '-sS', '-L', '--get', url]
            for (k, v) in headers.items():
                args.extend(['-H', ': '.join([k, v])])
            if out is None:
                # python3's subprocess.check_output returns stdout as a byte string
                result = subprocess.check_output(args)
                return result.decode('utf-8') if isinstance(result, bytes) else result
            else:
                print(" ".join(args))
                subprocess.call(args, stdout=out)
        except subprocess.CalledProcessError as e:
            print('curl GET error message: %' + (e.message if hasattr(e, 'message') else e.output), file=sys.stderr)
        return None


def sync_modis_dir(source, target_file_prefix, destination):
    """
    Function to retreive MODIS data if not already found in destination

    Args:
        source (str): HTTPS location
        target_file_prefix (str): MODIS prefix to be matched in the HTTPS location
        destination (str): Location to which file will be sent

    Returns:
        None

    """

    # Load all files in the src directory
    files = [f for f in csv.DictReader(io.StringIO(geturl('%s.csv' % source)), skipinitialspace=True)]

    # find matched file
    matched_file = [f for f in files if ".".join(f['name'].split(".")[:3]) == target_file_prefix]

    if matched_file:
        matched_file = matched_file[0]
        path = os.path.join(destination, matched_file['name'])
        url = source + '/' + matched_file['name']
        print(url)
        if not os.path.exists(path) or os.stat(path).st_size == 0:
            with open(path, 'w+b') as fh:
                geturl(url, fh)
    else:
        return None


def retrieve_modis(target_file_prefix):
    """
    Main function to get MODIS data

    Args:
        target_file_prefix (str): A MODIS filename prefix

    Returns:
        None

    """

    tag = target_file_prefix.split(".")[0]
    root = os.path.join("https://ladsweb.modaps.eosdis.nasa.gov/archive/allData/6/", tag)

    # build source path
    ydoy = target_file_prefix.split(".")[1].strip('A')
    y, doy = ydoy[0:4], ydoy[4:].zfill(3)
    source = os.path.join(root, y, doy)

    # make destination directory if it doesn't exist
    outpath = os.path.join(os.environ.get("MOD_DEST"), y, doy)
    if not os.path.exists(outpath):
        os.makedirs(outpath)

    # retrieve target file
    sync_modis_dir(source, target_file_prefix, outpath)
