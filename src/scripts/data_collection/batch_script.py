#!/home/users/dnfisher/.conda/envs/ltss/bin/python
import sys
from dotenv import load_dotenv, find_dotenv

from src.tools import data_collection_tools

# load dotenv
load_dotenv(find_dotenv())


def main():
    prefix = sys.argv[1]
    data_collection_tools.retrieve_modis(prefix)


if __name__ == "__main__":
    main()
