import os
from dotenv import load_dotenv, find_dotenv

from src.tools import data_collection_tools

load_dotenv(find_dotenv())


def main():
    # get MISR plume summary data
    misr_product_df = data_collection_tools.load_misr_phproj_df()

    misr_urls = [data_collection_tools.generate_misr_url(row) for i, row in misr_product_df.iterrows()]
    MOD02HKM_prefixes = list(set([data_collection_tools.generate_modis_l1_fname_prefix(row, tag="MOD02HKM.A")
                                  for i, row in misr_product_df.iterrows()]))
    MOD03_prefixes = list(set([data_collection_tools.generate_modis_l1_fname_prefix(row, tag="MOD03.A")
                               for i, row in misr_product_df.iterrows()]))
    MOD021KM_prefixes = list(set([data_collection_tools.generate_modis_l1_fname_prefix(row, tag="MOD021KM.A")
                                  for i, row in misr_product_df.iterrows()]))
    hv_data = data_collection_tools.load_hv()
    modis_prefixes = []
    for _, row in misr_product_df.iterrows():

        hh, vv = data_collection_tools.find_hv(hv_data, row.LatitudeAtSource, row.LongitudeAtSource)
        for h, v in zip(hh, vv):
            prefix = data_collection_tools.generate_modis_hv_fname_prefix(row, h, v, tag="MCD19A2.A")
            modis_prefixes.append(prefix)
    MCD19A2_prefixes = list(set(modis_prefixes))

    # check MISR presence
    actual_misr = 0
    for url in misr_urls:
        split_url = url.split('/')
        outpath = os.path.join(os.environ.get("MISR_PLUME_POLY_DEST"),
                               split_url[-3],
                               split_url[-1].replace('.txt', '_polygon.csv'))
        if os.path.exists(outpath):
            actual_misr += 1
    print("MISR proportion:", actual_misr / len(misr_urls))

    # MOD02HKM presence
    actual_MOD02HKM = 0
    for prefix in MOD02HKM_prefixes:
        try:
            ydoy = prefix.split(".")[1].strip('A')
            y, doy = ydoy[0:4], ydoy[4:].zfill(3)
            outpath = os.path.join(os.environ.get("MOD_DEST"), y, doy)
            prefixes = [".".join(f.split(".")[0:3]) for f in os.listdir(outpath)]
            if prefix in prefixes:
                actual_MOD02HKM += 1
        except Exception as e:
            print(e)
            continue

    print("MOD02HKM proportion:", actual_MOD02HKM / len(MOD02HKM_prefixes))

    # MOD021KM presence
    actual_MOD021KM = 0
    for prefix in MOD021KM_prefixes:
        try:
            ydoy = prefix.split(".")[1].strip('A')
            y, doy = ydoy[0:4], ydoy[4:].zfill(3)
            outpath = os.path.join(os.environ.get("MOD_DEST"), y, doy)
            prefixes = [".".join(f.split(".")[0:3]) for f in os.listdir(outpath)]
            if prefix in prefixes:
                actual_MOD021KM += 1
        except Exception as e:
            print(e)
            continue

    print("MOD021KM proportion:", actual_MOD021KM / len(MOD021KM_prefixes))

    # MOD03 presence
    actual_MOD03 = 0
    for prefix in MOD03_prefixes:
        try:
            ydoy = prefix.split(".")[1].strip('A')
            y, doy = ydoy[0:4], ydoy[4:].zfill(3)
            outpath = os.path.join(os.environ.get("MOD_DEST"), y, doy)
            prefixes = [".".join(f.split(".")[0:3]) for f in os.listdir(outpath)]
            if prefix in prefixes:
                actual_MOD03 += 1
        except Exception as e:
            print(e)
            continue

    print("MOD03 proportion:", actual_MOD03 / len(MOD03_prefixes))

    # MCD19A2 presence
    actual_MCD19A2 = 0
    for prefix in MCD19A2_prefixes:
        try:
            ydoy = prefix.split(".")[1].strip('A')
            y, doy = ydoy[0:4], ydoy[4:].zfill(3)
            outpath = os.path.join(os.environ.get("MOD_DEST"), y, doy)
            prefixes = [".".join(f.split(".")[0:3]) for f in os.listdir(outpath)]
            if prefix in prefixes:
                actual_MCD19A2 += 1
        except Exception as e:
            print(e)
            continue

    print("MCD19A2 proportion:", actual_MCD19A2 / len(MCD19A2_prefixes))


if __name__ == "__main__":
    main()
