import os
import sys
import tempfile
import subprocess
from dotenv import load_dotenv, find_dotenv

from src.tools import data_collection_tools

# load dotenv
load_dotenv(find_dotenv())


def retrieve_misr(urls):
    for url in urls:
        data_collection_tools.retreive_misr_plume_polygon(url, os.environ.get("MISR_PLUME_POLY_DEST"))


def retrieve_modis(prefixes, batch=False):
    for prefix in prefixes:
        if batch:
            # build the various paths required
            (gd, temp_file) = tempfile.mkstemp('.sh', 'ph.', os.environ.get("TEMP_DEST"), True)
            g = os.fdopen(gd, "w")
            g.write('#!/bin/bash\n')
            g.write('export PYTHONPATH=$PYTHONPATH:' + os.environ.get("PROJECT_PYTHON_PATH") + '\n')
            g.write(" ".join([os.path.join(os.environ.get("PROJECT_PYTHON_PATH"),
                                           "scripts",
                                           "data_collection",
                                           "batch_script.py"), prefix + "\n"]))
            g.write(" ".join(["rm -f ", temp_file + "\n"]))
            g.close()
            os.chmod(temp_file, 0o755)

            cmd = " ".join(['sbatch', '-p', 'short-serial',
                            '-o', os.path.join(os.environ.get("SLURM_OUT_DEST"), '%j.out'),
                            '-e', os.path.join(os.environ.get("SLURM_ERR_DEST"), '%j.err'),
                            temp_file])
            try:
                subprocess.call(cmd, shell=True)
            except Exception as e:
                print('Subprocess failed with error:', str(e))
        else:
            print(prefix)
            data_collection_tools.retrieve_modis(prefix)


def main():
    """
    Get list of products to download and send off misr and modis jobs form this script
    """
    try:
        tag = sys.argv[1]
        batch = True if sys.argv[2] == 'batch' else False
    except IndexError:
        print("Tag and batch must be provided and be one of: "
              "[MISR, MOD02HKM.A, MOD021KM.A, MOD03.A, MCD19A2.A] and [batch, ' ']")
        return

    # get MISR plume summary data
    misr_product_df = data_collection_tools.load_misr_phproj_df()

    # get list of MISR plume polygon data to be downloaded
    if tag == 'MISR':
        misr_urls = [data_collection_tools.generate_misr_url(row) for i, row in misr_product_df.iterrows()]
        retrieve_misr(misr_urls)
    elif tag in ["MOD02HKM.A", "MOD021KM.A", "MOD03.A"]:
        modis_prefixes = list(set([data_collection_tools.generate_modis_l1_fname_prefix(row, tag=tag)
                                   for i, row in misr_product_df.iterrows()]))
        retrieve_modis(modis_prefixes, batch=batch)
    elif tag in ["MCD19A2.A"]:
        hv_data = data_collection_tools.load_hv()
        modis_prefixes = []
        for _, row in misr_product_df.iterrows():
            hh, vv = data_collection_tools.find_hv(hv_data, row.LatitudeAtSource, row.LongitudeAtSource)
            for h, v in zip(hh, vv):
                prefix = data_collection_tools.generate_modis_hv_fname_prefix(row, h, v, tag="MCD19A2.A")
                modis_prefixes.append(prefix)
        modis_prefixes = list(set(modis_prefixes))
        retrieve_modis(modis_prefixes, batch=batch)
    else:
        print("Tag must be one of: [MISR, MOD02HKM.A, MOD021KM.A, MOD03.A, MCD19A2.A]")


if __name__ == "__main__":
    main()
