import os
import numpy as np
from dotenv import load_dotenv, find_dotenv
from collections import defaultdict

from src.tools import data_collection_tools

# load dotenv
load_dotenv(find_dotenv())


def main():

    misr_product_df = data_collection_tools.load_misr_phproj_df()

    annotation_combined_outpath = os.path.join(os.environ.get("ANNOTATION_COMBINED_DEST"), "annotations.txt")
    if os.path.exists(annotation_combined_outpath):
        os.remove(annotation_combined_outpath)

    # check processing completeness and produce single annotation file
    processed_plume_count = 0
    collapsed_bounding_boxes = 0
    for f in os.listdir(os.environ.get("ANNOTATION_DEST")):

        with open(os.path.join(os.environ.get("ANNOTATION_DEST"), f),
                  "r") as a:
            annotation = a.readlines()

        # split plumes
        split_plumes = str(annotation[0]).split(" ")
        num_boxes = (len(split_plumes) - 1) // 5
        good_plumes = []
        for i in range(num_boxes):
            xmin = split_plumes[1 + 5 * i]
            ymin = split_plumes[2 + 5 * i]
            xmax = split_plumes[3 + 5 * i]
            ymax = split_plumes[4 + 5 * i]
            c = split_plumes[5 + 5 * i]

            if ymax == ymin or xmax == xmin:
                collapsed_bounding_boxes += 1
                continue

            good_plumes += [xmin, ymin, xmax, ymax, c]

        if not good_plumes:
            continue
        good_plumes = " ".join([split_plumes[0]] + good_plumes)

        # count number of plumes
        plume_count = len(str(good_plumes).split(" ")) / 5
        processed_plume_count += plume_count

        # write annotation to single output file
        with open(annotation_combined_outpath, "a+") as a:
            a.write(good_plumes + "\n")

    # generate image statistics
    image_means_dict = defaultdict(float)
    image_sd_dict = defaultdict(float)
    image_dim_max = 0
    for n, f in enumerate(os.listdir(os.environ.get("ANNOTATION_IMAGE_DEST"))):

        try:
            arr = np.load(os.path.join(os.environ.get("ANNOTATION_IMAGE_DEST"), f))
            if max(arr.shape) > image_dim_max:
                image_dim_max = max(arr.shape)
        except Exception as e:
            print(e)
            continue

        for i, band in enumerate(arr.T):
            image_means_dict[i] += np.mean(band)
            image_sd_dict[i] += np.std(band)

    # report
    print("Processing completeness:", processed_plume_count / misr_product_df.shape[0])
    print(list((i, image_means_dict[i] / n, image_sd_dict[i] / n) for i in image_means_dict.keys()))
    print("Max subset size:", image_dim_max)
    print("Degenerate Plumes:", collapsed_bounding_boxes, collapsed_bounding_boxes / misr_product_df.shape[0])


if __name__ == "__main__":
    main()
