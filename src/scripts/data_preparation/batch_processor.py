import os
import tempfile
import subprocess
from dotenv import load_dotenv, find_dotenv

from src.tools import data_collection_tools, data_preparation_tools

# load dotenv
load_dotenv(find_dotenv())


def run(prefixes, batch=False):
    for prefix in prefixes:
        print(prefix)
        if batch:
            # build the various paths required
            (gd, temp_file) = tempfile.mkstemp('.sh', 'ph.', os.environ.get("TEMP_DEST"), True)
            g = os.fdopen(gd, "w")
            g.write('#!/bin/bash\n')
            g.write('export PYTHONPATH=$PYTHONPATH:' + os.environ.get("PROJECT_PYTHON_PATH") + '\n')
            g.write(" ".join([os.path.join(os.environ.get("PROJECT_PYTHON_PATH"),
                                           "scripts",
                                           "data_preparation",
                                           "batch_script.py"), prefix + "\n"]))
            g.write(" ".join(["rm -f ", temp_file + "\n"]))
            g.close()
            os.chmod(temp_file, 0o755)

            cmd = " ".join(['sbatch', '-p', 'short-serial',
                            '-o', os.path.join(os.environ.get("SLURM_OUT_DEST"), '%j.out'),
                            '-e', os.path.join(os.environ.get("SLURM_ERR_DEST"), '%j.err'),
                            temp_file])
            try:
                subprocess.call(cmd, shell=True)
            except Exception as e:
                print('Subprocess failed with error:', str(e))
        else:
            # prefix = "MOD02HKM.A2018243.0815"
            data_preparation_tools.prepare_annotations(prefix)


def main():

    """
    Get list of products to download and send off misr and modis jobs form this script
    """

    # get MISR plume summary data
    misr_product_df = data_collection_tools.load_misr_phproj_df()

    # get list of MODIS L1B datasets to be downloaded, dropping duplicates
    modis_prefixes = list(set([data_collection_tools.generate_modis_l1_fname_prefix(row, tag="MOD02HKM.A")
                               for i, row in misr_product_df.iterrows()]))

    # prepare the data
    run(modis_prefixes, batch=False)


if __name__ == "__main__":
    main()
