#!/home/users/dnfisher/.conda/envs/ltss/bin/python
import sys
from dotenv import load_dotenv, find_dotenv

from src.tools import data_preparation_tools

# load dotenv
load_dotenv(find_dotenv())


def main():
    prefix = sys.argv[1]
    data_preparation_tools.prepare_annotations(prefix)


if __name__ == "__main__":
    main()
