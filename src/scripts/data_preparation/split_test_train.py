import os
import random
from dotenv import load_dotenv, find_dotenv

# load dotenv
load_dotenv(find_dotenv())

if __name__ == "__main__":
    with open(os.path.join(os.environ.get("ANNOTATION_COMBINED_DEST"), "annotations.txt"), 'r') as f:
        combined_annotations = f.readlines()
    random.shuffle(combined_annotations)

    # get train/text index split
    test_proportion = 0.9
    n_samples = len(combined_annotations)
    split_index = int(n_samples * test_proportion)

    train_data = combined_annotations[:split_index]
    test_data = combined_annotations[split_index:]

    with open(os.path.join(os.environ.get("ANNOTATION_BASE_DEST"), "train.txt"), "w+") as f:
        f.writelines(train_data)

    with open(os.path.join(os.environ.get("ANNOTATION_BASE_DEST"), "val.txt"), "w+") as f:
        f.writelines(test_data)
