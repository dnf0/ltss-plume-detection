**Long Term Single Science - Plume Detection Dataset Generation**

This code contains the processes used to generate a labelled plume dataset for use with trianing CNN models using the YOLOv5 deep learning architecture.

The input data are the MISR plume height project to provide plume polygons and various L1 and L2 MODIS products from the Terra satellite (onboard which MISR and MODIS fly).  This produces a training dataset of ~40K hand labelled smoke plumes, with the plumes labelling format generated according to the YOLO and COCO spefications for use in either CNN architecture.  

A quick summary of the various modules:

[Data collection](https://gitlab.com/dnf0/ltss-plume-detection/-/tree/master/src/scripts/data_collection): runs the JASMIN SLURM batch processing for data collection.  From a set of MISR plumes polygons identifies the various MODIS L1B and MAIAC L2 products that are collocated with the MISR data and retrieves them using appropriate https protocols. 

[Data preparation](https://gitlab.com/dnf0/ltss-plume-detection/-/tree/master/src/scripts/data_preparation): runs the JASMIN SLURM batch processing to generate training data from the various data products.  Groups plumes based on distance, generating images with 1 or more plumes, scales imagery to 8bit range, generates RGBs and FCCs for processing.  
